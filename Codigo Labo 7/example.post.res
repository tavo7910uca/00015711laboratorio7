GiD Post Results File 1.0
Result "Velocity" "Load Case 1" 1 Vector OnNodes
ComponentNames "u" "v"
Values
1 27.6575 2
2 -7.39797 2
3 5 2
4 -4.43381 2
5 5 2
End values

Result "Pressure" "Load Case 1" 1 Scalar OnNodes
ComponentNames "p"
Values
1 3
2 3
3 -406293
4 -528183
5 -776210
End values
